﻿CREATE TABLE [dbo].[tblUserLogin] (
    [LoginId]  NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [UserName] VARCHAR (50) NULL,
    [Password] VARCHAR (50) NULL,
    [UserId]   NUMERIC (18) NULL,
    PRIMARY KEY CLUSTERED ([LoginId] ASC)
);

