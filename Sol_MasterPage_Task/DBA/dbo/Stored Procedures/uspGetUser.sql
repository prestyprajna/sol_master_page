﻿CREATE PROCEDURE uspGetUser
(
	@Command VARCHAR(MAX),
	--@UserId NUMERIC(18,0),
	--@FirstName VARCHAR(50),
	--@LastName  VARCHAR(50),
	--@ImagePath VARCHAR(MAX),

	@UserName VARCHAR(50),
	@Password VARCHAR(50),

	@Status INT OUT,
	@Message VARCHAR(MAX) OUT
)
AS
	BEGIN

		--declaration of variables
		DECLARE @ErrorMessage VARCHAR(MAX)
		DECLARE @UserId NUMERIC(18,0)
		--DECLARE @tempUserName VARCHAR(50)
		--DECLARE @tempPassword VARCHAR(50)

		IF @Command='SELECT'
		BEGIN

			BEGIN TRANSACTION

				BEGIN TRY

					SELECT @UserId=UL.UserId
						FROM tblUserLogin AS UL
							WHERE UL.UserName=@UserName AND UL.Password=@Password

					IF @UserId IS NOT NULL
						BEGIN

							SELECT U.UserId,
							U.FirstName,
							U.LastName,
							U.ImagePath
								FROM tblUser AS U
									INNER JOIN
										tblUserLogin AS UL
											ON
												U.UserId=UL.UserId
													WHERE U.UserId=@UserId

							SET @Status=1
							SET @Message='SELECT SUCCESSFULL'
						END
					ELSE
						BEGIN
							SET @Status=0
							SET @Message='USERNAME AND PASSWORD DOES NOT MATCH'

						END


					--SELECT U.UserId,
					--U.FirstName,
					--	U.LastName,
					--	U.ImagePath
					--		FROM tblUser AS U
					--			WHERE U.UserId IN(
					--								SELECT UL.UserId
					--									FROM tblUserLogin AS UL
					--										WHERE UL.UserName=@UserName AND UL.Password=@Password
					--							)					
					
			
					

					COMMIT TRANSACTION
				END TRY

				BEGIN CATCH

					SET @ErrorMessage=ERROR_MESSAGE()
					SET @Status=0
					SET @Message='SELECT EXCEPTION'
					ROLLBACK TRANSACTION
					RAISERROR(@ErrorMessage,16,1)

				END CATCH

		END


	END
