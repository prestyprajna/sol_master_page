﻿<%@ Page Language="C#" AutoEventWireup="true" Async="true" CodeBehind="LoginPage.aspx.cs" Inherits="Sol_MasterPage_Task.LoginPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <style type="text/css">
        .txtClass
        {
            /*margin:auto;*/
            height:20px;
            width:250px;
            padding:10px;
            border-radius:10px;
        }

        table
        {
            margin:auto;
            margin-top:200px;
            
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <asp:ScriptManager ID="scriptManager" runat="server"></asp:ScriptManager>

        <asp:UpdatePanel ID="updatePanel" runat="server">
            <ContentTemplate>
                
                <table>
                    <tr>
                        <td>
                            <asp:TextBox ID="txtUserName" runat="server" Placeholder="UserName" CssClass="txtClass"></asp:TextBox>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <asp:TextBox ID="txtPassword" runat="server" Placeholder="Password" CssClass="txtClass"></asp:TextBox>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <asp:Button ID="btnLogin" runat="server" Text="Log In" Font-Size="20px" OnClick="btnLogin_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblErrorMessage" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                </table>


            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnLogin" />
            </Triggers>
        </asp:UpdatePanel>
    
    </div>
    </form>
</body>
</html>
