﻿using Newtonsoft.Json;
using Sol_MasterPage_Task.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_MasterPage_Task
{
    public partial class Main : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["UserInfo"]!=null)
            {
                string jsonData = Session["UserInfo"] as string;

                UserEntity userEntityObj= JsonConvert.DeserializeObject<UserEntity>(jsonData);

                userEntityObj.FullName = new StringBuilder()
                                        .Append(userEntityObj?.FirstName)
                                        .Append(" ")
                                        .Append(userEntityObj?.LastName)
                                        .ToString();

                lblFullName.Text = userEntityObj.FullName;

                imgPath.ImageUrl=userEntityObj.ImagePath;

                //Server.Transfer("~/View/Pages/UserProfile.aspx", false);

            }
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            //destroy session
            Session.Remove("UserInfo");
            Session.Abandon();

            Server.Transfer("~/LoginPage.aspx", false);
        }

        
    }
}