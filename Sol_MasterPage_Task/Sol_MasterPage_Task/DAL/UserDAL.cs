﻿using Sol_MasterPage_Task.DAL.ORD;
using Sol_MasterPage_Task.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Sol_MasterPage_Task.DAL
{
    public class UserDAL
    {
        #region  declaration
        private UserDCDataContext dc = null;
        #endregion

        #region  constructor
        public UserDAL()
        {
            dc = new UserDCDataContext();
        }
        #endregion

        #region  public methods

        public async Task<dynamic> GetUserData(UserEntity userEntityObj)
        {
            int? status = null;
            string message = null;
                
            return await Task.Run(() =>
            {
                var getQuery =
                dc?.uspGetUser(
                    "SELECT",
                    userEntityObj?.loginEntityObj?.UserName,
                    userEntityObj?.loginEntityObj?.Password,
                    ref status,
                    ref message
                    )
                    ?.AsEnumerable()
                    ?.Select((leUserEntityObj) => new UserEntity()
                    { 
                        UserId=leUserEntityObj?.UserId,                       
                        FirstName = leUserEntityObj?.FirstName,
                        LastName = leUserEntityObj?.LastName,
                        ImagePath = leUserEntityObj?.ImagePath
                    })
                    ?.FirstOrDefault();

                return (status == 1) ? getQuery : (dynamic)message;


            });
        }

        #endregion
    }
}