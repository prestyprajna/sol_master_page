﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_MasterPage_Task.Entity
{
    public class UserLoginEntity
    {
        public decimal? LoginId { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public decimal? UserId { get; set; }
    }
}