﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_MasterPage_Task.Entity
{
    public class UserEntity
    {
        public decimal? UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string ImagePath { get; set; }

        public string FullName { get; set; }

        public UserLoginEntity loginEntityObj { get; set; }
    }
}