﻿using Newtonsoft.Json;
using Sol_MasterPage_Task.DAL;
using Sol_MasterPage_Task.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_MasterPage_Task
{
    public partial class LoginPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected async void btnLogin_Click(object sender, EventArgs e)
        {
            // UserEntity result=await new UserDAL().GetUserData(this.DataMapping().Result);

            UserEntity userEntityObj = new UserEntity()
            {
                loginEntityObj = new UserLoginEntity()
                {
                    UserName = txtUserName.Text,
                    Password =txtPassword.Text
                }
            };

            var result = await new UserDAL().GetUserData(userEntityObj);

            if(result is UserEntity)
            {
                //store in session
                string jsonData = JsonConvert.SerializeObject(result);
                Session["UserInfo"] = jsonData;

                //redirect to user profile page
                Server.Transfer("~/View/Pages/UserProfile.aspx", false);
            }
            else
            {
                lblErrorMessage.Text = result;
            }

          
        }


        #region  private methods

        //private async Task<UserEntity> DataMapping()
        //{
        //    return  await Task.Run(() =>
        //    {
        //        UserEntity userEntityObj = new UserEntity()
        //        {
        //            loginEntityObj = new UserLoginEntity()
        //            {
        //                UserName = this.UserNameBinding,
        //                Password = this.PasswordBinding
        //            }
        //        };

        //        return userEntityObj;
        //    });
        //}

        #endregion

        #region  public property

        public string UserNameBinding
        {
            get
            {
                return
                    txtUserName.Text;
            }
            set
            {
                txtUserName.Text = value;
            }
        }

        public string PasswordBinding
        {
            get
            {
                return
                    txtPassword.Text;
            }
            set
            {
                txtPassword.Text = value;
            }
        }

        #endregion
    }
}